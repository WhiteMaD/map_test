window.onload = function() {
	var map = L.map('map', {measureControl:true}, {drawControl: true}).setView([47.24087, 39.71068], 17); // стартовые координаты на карте

	L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { // шаблон для загрузки тайлов
		attribution: 'Тестовое задание &copy; <a rel="nofollow" href="http://osm.org/copyright">OpenStreetMap</a> contributors'
	}).addTo(map);

	var LeafIcon = L.Icon.extend({ // параметры иконки, для отображения на карте
			options: {
				shadowUrl: 'icons/shadow.png',
				iconSize:     [32, 37],
				shadowSize:   [41, 41],
				iconAnchor:   [16,37],
				shadowAnchor: [16, 37],
				popupAnchor:  [0, -30]
			}
    });

	var churchIcon = new LeafIcon({iconUrl: 'icons/church.png'}), // файлы изображений для каждой иконки
		dormitoryIcon = new LeafIcon({iconUrl: 'icons/dormitory.png'}),
		playgroundIcon = new LeafIcon({iconUrl: 'icons/playground.png'}),
		stadiumIcon = new LeafIcon({iconUrl: 'icons/stadium.png'}),
		workshopIcon = new LeafIcon({iconUrl: 'icons/workshop.png'}),
		supermarketIcon = new LeafIcon({iconUrl: 'icons/supermarket.png'}),
		dogsIcon = new LeafIcon({iconUrl: 'icons/dogs.png'}),
		monumentIcon = new LeafIcon({iconUrl: 'icons/monument.png'});

	L.marker([47.23928, 39.71115], {icon: churchIcon}) // добавление маркеров, с соответсвующей иконкой
		.bindPopup("Храм Святой Великомученицы Татьяны").addTo(map);
	L.marker([47.23939, 39.71274], {icon: dormitoryIcon})
		.bindPopup("Общежитие РГСУ").addTo(map);
	L.marker([47.23987, 39.71106], {icon: playgroundIcon})
		.bindPopup("Детская площадка").addTo(map);
	L.marker([47.23882, 39.70784], {icon: supermarketIcon})
		.bindPopup("Пятерочка, сеть супермаркетов").addTo(map);
	L.marker([47.24197, 39.70958], {icon: dogsIcon})
		.bindPopup("Площадка для дрессировки собак").addTo(map);
	L.marker([47.24099, 39.70914], {icon: stadiumIcon})
		.bindPopup("Стадион ДГТУ").addTo(map);
	L.marker([47.24272, 39.71278], {icon: workshopIcon})
		.bindPopup("VIRBACauto, сеть автотехцентров").addTo(map);
	L.marker([47.23951, 39.71044], {icon: monumentIcon})
		.bindPopup("Памятник студентам и преподавателям РИСХМА, погибшим в годы Великой Отечественной войны").addTo(map);
	
	L.control.mousePosition().addTo(map); // плагин, показывающий координаты на позиции курсора
}